module bitbucket.org/uwaploe/abprsession

go 1.15

require (
	bitbucket.org/uwaploe/abprdata v0.9.9
	github.com/golang/protobuf v1.4.3
	github.com/urfave/cli v1.22.5
	go.etcd.io/bbolt v1.3.5
)
