// Read an ABPR session file
package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"time"

	"bitbucket.org/uwaploe/abprdata"
	"github.com/golang/protobuf/proto"
	"github.com/urfave/cli"
	bolt "go.etcd.io/bbolt"
)

var Version = "dev"
var BuildDate = "unknown"

type event struct {
	Name string      `json:"name"`
	Data interface{} `json:"data"`
}

func main() {
	app := cli.NewApp()
	app.Name = "abprsession"
	app.Usage = "Read an ABPR session file"
	app.Version = Version

	app.Flags = []cli.Flag{
		cli.VersionFlag,
	}

	app.Commands = []cli.Command{
		{
			Name:      "events",
			Usage:     "extract event log from `FILE`",
			ArgsUsage: "FILE",
			Action: func(c *cli.Context) error {
				if c.NArg() < 1 {
					return cli.NewExitError("missing arguments", 1)
				}
				db, err := bolt.Open(c.Args().Get(0), 0644,
					&bolt.Options{Timeout: 1 * time.Second})
				if err != nil {
					return err
				}
				err = db.View(func(tx *bolt.Tx) error {
					b := tx.Bucket([]byte("events"))
					if b == nil {
						return errors.New("No event log available")
					}
					b.ForEach(func(k, v []byte) error {
						rec := event{}
						err := json.Unmarshal(v, &rec)
						if err != nil {
							return err
						}
						fmt.Printf("%s [%s] %v\n", k, rec.Name, rec.Data)
						return nil
					})
					return nil
				})
				return err
			},
		},
		{
			Name:      "data",
			Usage:     "export data from `FILE` in CSV format",
			ArgsUsage: "FILE",
			Action: func(c *cli.Context) error {
				if c.NArg() < 1 {
					return cli.NewExitError("missing arguments", 1)
				}
				db, err := bolt.Open(c.Args().Get(0), 0644,
					&bolt.Options{Timeout: 1 * time.Second})
				if err != nil {
					return err
				}
				fmt.Println("time,pressure,temperature")
				err = db.View(func(tx *bolt.Tx) error {
					b := tx.Bucket([]byte("stored-data"))
					if b == nil {
						return errors.New("No data available")
					}
					b.ForEach(func(k, v []byte) error {
						rec := &abprdata.Record{}
						err := proto.Unmarshal(v, rec)
						if err != nil {
							return err
						}
						fmt.Println(rec.AsCsv())
						return nil
					})
					return nil
				})
				return err
			},
		},
		{
			Name:      "status",
			Usage:     "export deployment status records from `FILE` in CSV format",
			ArgsUsage: "FILE",
			Action: func(c *cli.Context) error {
				if c.NArg() < 1 {
					return cli.NewExitError("missing arguments", 1)
				}
				db, err := bolt.Open(c.Args().Get(0), 0644,
					&bolt.Options{Timeout: 1 * time.Second})
				if err != nil {
					return err
				}
				fmt.Println("time,pressure,temperature,pitch,roll,speed")
				err = db.View(func(tx *bolt.Tx) error {
					b := tx.Bucket([]byte("deployment-status"))
					if b == nil {
						return errors.New("No status available")
					}
					b.ForEach(func(k, v []byte) error {
						rec := &abprdata.Status{}
						err := proto.Unmarshal(v, rec)
						if err != nil {
							return err
						}
						fmt.Println(rec.AsCsv())
						return nil
					})
					return nil
				})
				return err
			},
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
	}
}
